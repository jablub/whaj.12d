(defproject whaj "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :plugins [[lein-localrepo "0.5.2"]]
  :dependencies [[org.clojure/clojure "1.5.1"] 
                 [incanter "1.5.1"]
                 [org.clojure/math.numeric-tower "0.0.2"]
                 [org.clojure/math.combinatorics "0.0.4"]
                 [criterium "0.4.1"]
                 [net.mikera/core.matrix "0.9.0"]
                 ]
  ;:jvm-opts ["-Xms4G" "-Xmx7G" "-XX:-UseGCOverheadLimit"]
  )

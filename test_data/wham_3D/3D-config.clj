

(def root "/home/andrew/git/whaj.12D/test_data/wham_3D/")

; Min and max per dimension
;[ 0.856756448745728 2.90161752700806 ]
;[ -1.34243750572205 2.32785224914551 ]
;[ -1.70684254169464 3.74990844726562 ]

{
 :pmf "dummy.clj"
 :file-prefix "trj_"
 :filename-fn (fn [H# uc] (str "trj_" H# ".dat"))
 :line-parse-fn (fn [line] (str-to-doubles line))
 :dir-hist-overide "/"
 :D 3
 :k k_B_KCal
 :HS [

      {
       :T 300
       :H [31]
       :H-bounds [
                  [ 0.856756448745728 2.90161752700806 ]
                  [ -1.34243750572205 2.32785224914551 ]
                  [ -1.70684254169464 3.74990844726562 ]
                  ]
       :UC (map #(str-to-doubles (slurp (str root "centers_" % ".dat" ))) (take 31 (range)))
       :K [10 10 10]
;       :numeric-step [1 1]
;       :no-samples 500
       :BS [[10 10 10]]
       :imp {
             :name :wham_c
             :version "gpu_cpu"
             :no-ofruns 1
             :tol 0.001
             :max-iterations 10000
             :debug false
             } 
       }
      ]
 }

{
 :pmf "butane-2D.clj"
 :file-prefix "prod_"
 :D 2
 :k k_B_KCal
 :out-file "CPU"
 :HS [

      {
       :T 300
       :H [19 19]
       :H-bounds [[-180 180][-180 180]]
       :UC (umbrella-centers-ND [[-180 180] [-180 180]] [20 20])
       :K [0.001 0.001]
       :numeric-step [0.5 0.5]
       :no-samples 50000
       :BS [[30 30]]
       :imp {
             :name :wham_c
             :version "gpu_cpu"
             :no-ofruns 1
             :tol 0.001
             :max-iterations 5000
             :debug false
             } 
       }

      ]
 }

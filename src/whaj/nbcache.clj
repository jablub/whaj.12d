(ns whaj.nbcache
  (:use [clojure.java.io :as io])
  (:use clojure.pprint)
  (:use clojure.test)
  (:require [clojure.math.combinatorics :as combo])
  (:require [clojure.math.numeric-tower :as math])  
  (:use whaj.common)
  )


(defn nbcache [dir-hist filename-fn line-parse-fn D H H-bounds UC B]
  (println "Generating NH and NB caches ")
  (println "Input parameters ")
  (println "dimensions           : " D)
  (println "number of histograms : " H )
  (println "bounds of histograms : " H-bounds)
  (println "umbrella centers     : " UC)
  (println "number of bins       : " B)
  
  ; define all other globals
  (let [
        dir-bin (str dir-hist (dir-bin B) )                        
        file-out-NB (str dir-bin "NB")                
        file-out-NH (str dir-bin "NH")                
        NH          (int-array (reduce * H))
        NB          (int-array (reduce * B))
        B-widths    (bin-width-ND H-bounds B)
        H#          (atom 0N)
        ]
    
    (println "NH size           : " (count NH))
    (println "NB size           : " (count NB))
    (println "bin widths           : " B-widths)
    (println "histogram directory  : " dir-hist)
    (println "bin directory        : " dir-bin)
    (make-parents (str dir-bin "/dummy-file"))
    
    
    (doseq [uc UC]
      ( let [
             filename (str dir-hist (filename-fn @H# uc))
             ]
        (print "Reading samples for histogram #:" @H# " centered at: " uc " from file " filename )
        (if (.exists (io/file filename))

          (with-open [rdr (io/reader filename)]
            (doseq [line (line-seq rdr)]
              
              (if-not (clojure.string/blank? line)
                
                (do 
                  ;; Add each nd-sample to the NB array
                  (let [
                        points (line-parse-fn line)
                        bin-index (bin-index-ND H-bounds B-widths points)
                        ]
                    (if bin-index
                      (inc-int-array-1D NB (flatten-ND B bin-index))
                      )
                    ;            (println  "|" line "|"  points "|"  bin-index "|"  flat-bin-index "|"  "|"  )
                    )
                  
                  ;; Add each nd-sample to the NH array
                  (inc-int-array-1D NH @H#)
                  
                  )
                )
              )
            )
          (print " ..... Does not exist")
          
          )
        )
      (println " ..... Done")
      
      ;; increment the histogram number counter
      (swap! H# inc)
      )
;    (pprint NB)
;    (pprint NH)
    (println "Writing : " file-out-NB)
    (write-seq-1D file-out-NB NB)
    
    (println "Writing : " file-out-NH)
    (write-seq-1D file-out-NH NH)
    )
  )



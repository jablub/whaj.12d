(ns whaj.ccache
  (:use [clojure.java.io :as io])
  (:use clojure.pprint)
  (:use clojure.test)
  (:require [clojure.math.combinatorics :as combo])
  (:require [clojure.math.numeric-tower :as math])
  (:use whaj.common)
  (:use whaj.constants)
  )


(defn
     ^{:test (fn[] 
             (assert (= (hefty 300 k_B_KCal [0.1 0.1] [0.5 0.5] [0.5 2.5]) 0.7144774143069742 ))  ;; Wolfram e ^ (-1/(0.001982923700 * 300) * ( 1/2 * 0.1 * (0.5 -0.5)^2 + 1/2 * 0.1 * (0.5 -2.5)^2  )
             )}
  hefty 
  "Calculates the biasing factor given Temp, Boltzmann constant, nd spring factor, nd umbrella center and nd point"
  [T k K bc uc]
  (let  [β (/ -1 (* k T))] 
    (Math/exp (* β (reduce + (map #(* 1/2 %1 (- %2 %3) (- %2 %3)) K bc uc))) )
    )
  
  )


(defn 
  ^{:test (fn[] 
            (assert (= (bin-centers-1D [-2 2] 4) '(-3/2 -1/2 1/2 3/2) ))
            )}
bin-centers-1D 
  [H-bounds B]
  
  (let [
        bin-width (/ (- (second H-bounds) (first H-bounds)) B)
        half-bin-width (/ bin-width 2)
        first-bc (+ (first H-bounds) half-bin-width)
        ]
    (range first-bc (second H-bounds) bin-width)
    )
  )

(defn 
  ^{:test (fn[] 
            (assert (= (bin-centers-ND [[-2 2]] [4]) '((-3/2) (-1/2) (1/2) (3/2)) ))
            (assert (= (bin-centers-ND [[0 4] [0 4]] [4 4]) '((1/2 1/2) (1/2 3/2) (1/2 5/2) (1/2 7/2) (3/2 1/2) (3/2 3/2) (3/2 5/2) (3/2 7/2) (5/2 1/2) (5/2 3/2) (5/2 5/2) (5/2 7/2) (7/2 1/2) (7/2 3/2) (7/2 5/2) (7/2 7/2)) ))
            )}
bin-centers-ND 
  "Returns a list of nd points which represent the centers of the nd bins"
  [H-bounds B]
  (apply combo/cartesian-product 
         (map bin-centers-1D H-bounds B))
  )

(run-tests)


(defn
  ccache 
  ([dir-bin k T D H H-bounds B UC K]
    (println "Generating C cache ")
    (println "Input parameters ")
    (println "temperature          : " T)
    (println "boltzmann constant   : " k)
    (println "dimensions           : " D)
    (println "number of histograms : " H )
    (println "bounds of histograms : " H-bounds)
    (println "number of bins       : " B)
    (println "umbrella  centers    : " UC)
    (println "spring constants     : " K)
    
    ; define all other globals
    (let [                
          file-out-C  (str dir-bin "C")                                             
          B-widths    (bin-width-ND H-bounds B)
          B-centers   (bin-centers-ND H-bounds B)
          ]
      (println "no. of bin centers   : " (count B-centers))
;      (println "bin widths           : " B-widths)
      (println "bin directory        : " dir-bin)
      (make-parents (str dir-bin "/dummy-file"))

      
      (println "No. UC : " (count UC))
      (println "Writing : " file-out-C)
      (doall 
        (for [uc UC]
          (do
            (println "Writing : " uc)
            (def C 
              (for [bc B-centers]
                (hefty T k K bc uc)))
            (println "Size of C : " (count C))
            (append-seq-1D file-out-C C)
            )
          )
        )   
      (println "  ....Done")
      )
    )
  )

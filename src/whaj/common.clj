(ns whaj.common
  (:require [clojure.math.numeric-tower :as math])  
  (:use [clojure.java.io :as io])
  (:use clojure.test)
  )

(defn 
  ^{:test (fn[] 
            (assert (= "3" (dir-shape [3])))
            (assert (= "2x3" (dir-shape [2 3])))
            )}
dir-shape
  "Creates a string representation of a square ndarray shape in the form %x%... Used for histogram and bin directories."
  ([coll]
    (apply str (interpose "x" coll)))
  )

(defn dir-bin [B] (str "B" (dir-shape B) "/") )

(defn dir-hist [H K no-samples numeric-step] (str "H" (dir-shape H) "-K" (dir-shape K) "-S" no-samples "-ns" (dir-shape numeric-step) "/"))

(defn 
  ^{:test (fn[] 
            (assert (= (map #(= %1 %2) '(1.2 4.0 -3.4) (list (str-to-doubles " 1.2   4   -3.4 ")))))  
            )}
str-to-doubles
  "Converts a single string with one or more doubles into a list of doubles"
  [string]
  (map #(Double/parseDouble %)
       (clojure.string/split (clojure.string/trim string) #"\s+")))


(defn 
  ^{:test (fn[] 
            (assert (= (bin-width-1D [-12 12] 12 )  2) )  
            )}
bin-width-1D 
  "Divides the bounds of a one dimensional axis into equally spaced bin widths"
  [bounds noOfBins]
  (/ (- (second bounds) (first bounds)) noOfBins )) 


(defn
  ^{:test (fn[] 
            (assert (= (bin-width-ND [[-5 5]] [5])                     '(2)) )  
            (assert (= (bin-width-ND [[-5 5] [-4 4]] [5 5])            '(2 8/5))  )  
            (assert (= (bin-width-ND [[-5 5] [-4 4] [-3 3]] [5 5 5])   '(2 8/5 6/5)) )  
            )}  
bin-width-ND 
  "Divides the bounds of an n dimensional axis into equally spaced bin widths."
  [bounds noOfBins]
  (map bin-width-1D bounds noOfBins))


(defn
  ^{:test (fn[] 
            (assert (= (in-range-1D [-5 5] -5) true) )  
            (assert (= (in-range-1D [-5 5] 0)  true) )  
            (assert (= (in-range-1D [-5 5] 5)  false) )  
            )}  
in-range-1D 
  [bounds point]
  "Indicates if a value is within a range inclusive of lower bound exclusive of upper"
  (and (>= point (first bounds)) (< point (second bounds))))


(defn 
  ^{:test (fn[] 
            (assert (= (in-range-ND [[-5 5]] [0])  true) )  
            (assert (= (in-range-ND [[-5 5] [-3 3]] [0 0])  true) )  
            (assert (= (in-range-ND [[-5 5] [-3 3] [-4 4]] [0 0 3])  true) )  
            )}  
in-range-ND
  "Indicates if a value is within a range inclusive of lower bound exclusive of upper - applies to n dimensional"
  [bounds point]
  (reduce #(and %1 %2) 
          (map in-range-1D bounds point)))




(defn 
  ^{:test (fn[] 
            (assert (= (bin-index-1D [-5 5] 1 -5.5)  nil) )  
            (assert (= (bin-index-1D [-5 5] 1 -5)  0) )  
            (assert (= (bin-index-1D [-5 5] 1 4.6)  9.0) )  
            )}  
bin-index-1D 
  "Returns the bin index of a point given the lower bound and the bin width along one dimension. Inclusive lower bound exclusice upper bound. Returns nil if out of bounds."
  [bounds binwidth point]
  (if (in-range-1D bounds point) 
    (math/floor (/ (- point (first bounds)) binwidth))))


(defn 
  ^{:test (fn[] 
            (assert (= (bin-index-ND [[-3 3]] [1] [-1])  '(2)) )  
            (assert (= (bin-index-ND [[-5 5] [-3 3]] [1 1] [-1 -1])  '(4 2)) )  
            (assert (= (bin-index-ND [[-5 5] [-3 3] [-4 4]] [1 1 1] [-1 -1 2])  '(4 2 6)) )  
            )}  
bin-index-ND 
  "Returns the bin index of a point given the lower bounds and the bin widths along n dimensions. Inclusive lower bound exclusice upper bound. Returns nil if out of bounds."
  [bounds binwidths point]
  (if (in-range-ND bounds point) 
    (map bin-index-1D bounds binwidths point)
    (println "\n *-- Point " point "out of bounds " bounds)
    )
  )

(defn single? [coll]
  (= 1 (count coll)))

(defn 
  ^{:test (fn[] 
            (assert (= (flatten-ND [8 6 4] [4 3 2])  124) )    ; [X Y Z]  [x y z]     ->         z*(X*Y) + y*X + x       -> (2* (8 * 6 )) +  (3 * 8) + 4 = 124             
            (assert (= (flatten-ND [8 6] [4 3])      28 ) )    ; [X Y]  [x y]         ->         y*X + x                 ->                  (3 * 8) + 4 =  28             
            (assert (= (flatten-ND [8] [4])          4  ) )    ;  X x                 ->         x                       ->                            4 =   4             
            )}
flatten-ND 
  "Flattens an n-dimensional index according to the shape of the array." 
  [shape index]
;  (println "shape " shape " index " index)
  (if (single? index) (first index)
    (+ 
      (reduce * (last index) (butlast shape)) 
      (flatten-ND (butlast shape) (butlast index)))))


(defn inc-int-array-1D 
  "Increment the value of a 1-D array, by one, at given index "
  [array index]
  (aset-int array index (+ 1 (aget array index))) 
  )


(defn write-seq-1D 
  [filename array]
  (with-open [wrt (io/writer filename)]
    (doseq [x array]
      (.write wrt (str x "\n")))))

(defn append-seq-1D 
  [filename array]
  (with-open [wrt (io/writer filename :append true)]
    (doseq [x array]
      (.write wrt (str x "\n")))))


(defn
  ^{:test (fn[] 
            (assert (= (uc-filename "test_" 2 [20])  "test_20") )  
            (assert (= (uc-filename "test_" 2 [20 30])  "test_20_30") )  
            )}  
  uc-filename 
  [file-prefix H# uc] 
  (str file-prefix (apply str (interpose "_" uc))
       )
  )

(defn 
  ^{:test (fn[] 
            (assert (= (H#-filename "test_" 4 [20 30])  "test_4") )  
  
            )}  
  H#-filename 
  [file-prefix H# uc] 
  (str file-prefix H#)
  )



(run-tests)


(ns whaj.emulator
  (:use clojure.test)
  (:use clojure.repl)
  (:use [clojure.java.io :as io])
  (:require [clojure.math.combinatorics :as combo])
  (:require [clojure.math.numeric-tower :as math])
  (:use whaj.constants)
  )



(defn 
  ^{:test (fn[] 
            (assert (= (harmonic-ND [2] [2] [2])  0.0) )  
            (assert (= (harmonic-ND [3 2 1] [1 1 1] [3 4 5])  46.0) )  
            )}  
harmonic-ND
  [K ξ   point]
  (reduce + (map #(* %1 (Math/pow (- %2 %3) 2) ) K ξ   point) )
  )


(defn 
  ^{:test (fn[] 
            (assert (= (boltzmann k_B_KCal 300 2) 0.034664508474528816) )  
            )}  
  boltzmann
  [k T x]
  (Math/exp (* (/ -1 (* k T)) x)  )
  )

(defn
  umbrella-probability-ND
  [pmf k T K ξ]
  (fn [point]
    ((partial boltzmann k T) (+ ((partial harmonic-ND K ξ) point) (pmf point)))
    )
  )

(defn 
  ^{:test (fn[] 
             (assert (= (trapezium-ND (fn [point] 2) [[0 2] [0 2]])   8))  
             (assert (= (trapezium-ND (fn [point] 2) [[0 2]])         4))  
             )}   
  trapezium-ND 
  "The area of a trapezium is the width * the average of the heights"
  [f bounds]
  (let [points (apply combo/cartesian-product bounds)
        average-of-heights (/ (reduce + (map f points)) (count points) )
        base (reduce * (map #(- (second %) (first %)) bounds)) ]
    
;    (println "points   : " points)
;    (println "heights  : " (map f points))
;    (println "average  : " average-of-heights)
;    (println "base     : " base)
    (* base average-of-heights)
    )
  )


(defn 
  ^{:test (fn[] 
            (assert (= (inclusive-range-1D [0 1] 0.5))  '(0 0.5 1))
            )}   
inclusive-range-1D
  "similar to range but includes the end. used to divide base up into segments to calculate vloume"
  [bound step]
  (flatten (list (range (first bound) (second bound) step) (second bound))))

(defn 
  ^{:test (fn[] 
            (assert (= (split-bounds-ND [[0 1]] [0.5])            '(((0 0.5)) ((0.5 1)))) )  
            (assert (= (split-bounds-ND [[0 1] [0 1]] [0.5 0.5])  '(((0 0.5) (0 0.5)) ((0 0.5) (0.5 1)) ((0.5 1) (0 0.5)) ((0.5 1) (0.5 1))) ))  
            )}  
split-bounds-ND
  "Split the nd bounds up. Will give a list of single bounds for 1D functions and a list of double bounds for 2D functions. Used as the base when calculating area."
  [bounds step]
  (apply combo/cartesian-product (map #(partition 2 1 (inclusive-range-1D %1 %2)) bounds step))
  )


(defn 
  ^{:test (fn[] 
            (assert (= (volume-ND #(reduce + %) [[0 1]] [0.5])  0.5) )  
            (assert (= (volume-ND #(reduce + %) [[0 1] [0 1]] [0.5 0.5])  1.0) )  
            (assert (= (volume-ND #(reduce + %) [[0 1] [0 1] [0 1]] [0.5 0.5 0.5])  1.5) )  
            )}   
volume-ND
  "Sum the volume of all the trapeziums obtained by splitting the bounds up into their respective steps. Assumes f is always positive."
  [f bounds step]
  (reduce + (map #(trapezium-ND f %) (split-bounds-ND bounds step)))
  )

(defn 
  ^{:test (fn[] 
            (assert (= (volume-ND (normalize #(reduce + %) [[0 3]] [0.5]) [[0 3]] [0.5])  1.0) )  ; Volume of normalized function between bounds of normalization should always be 1  
            )}   
normalize
  "Normalizes a probability distribution by returning the function divided by the hypervolume."
  [f bounds step]
  (let [vol (volume-ND f bounds step)]
    (comp #(* (/ 1 vol) %) (partial f))
    )
  )



(defn 
  ^{:test (fn[] 
            (assert (= (get-points-ND [[2 3][2 3]] [0.5])  '((2) (2.5) (3))) )  
            (assert (= (get-points-ND [[2 3][2 3]] [0.5 0.5])  '((2 2) (2 2.5) (2 3) (2.5 2) (2.5 2.5) (2.5 3) (3 2) (3 2.5) (3 3)) ))  
            )}   
get-points-ND
  "Gets all the points between the bounds (inclusive) separated by step"
  [bounds step]
  (apply combo/cartesian-product (map #(inclusive-range-1D %1 %2) bounds step ))
  )


(defn 
  ^{:test (fn[] 
            (assert (= (reduce-bounds #(*  (first %) (first %) (first %)) [[0 10]] [1])                '((3 10)) ) )  
            (assert (reduce-bounds #(*  (first %) (first %) (first %)) [[0 2] [0 2]] [1 1])          '((1 2) (0 2)) )  
            )}   
reduce-bounds  ;; call this rebound - it's funnier
  "Creates a map of all points and corresponding value f. Filters out all points below the threshold (1% of the height) and returns new bounds enclosing f into the threshold."
  [f bounds step]
  (let [points (get-points-ND bounds step)
        vals (map f points)
        maxval (reduce max vals)
        minval (reduce min vals)
        threshold (* 0.01 (- maxval minval))  ;; one percent of the diffence 
        threshold-points (filter #(> (val %) threshold) (apply assoc {} (interleave points vals)))   ;; creates a map of {point value} filtering out those below threshold
        ] 
    (if (empty? threshold-points) 
      [] ; return empty set since no points reducing bounds means no samples
      (let [
            dimension-lists (apply map list (map #(first %) threshold-points ))                          ;; creates n lists of values of in specific dimension
            new-bounds (map #(list (apply min %) (apply max %)) dimension-lists)
            ]
;        (println "points: " points)
;        (println "vals: " vals)
;        (println "threshold-points: " threshold-points)
;        (println "dimension-lists: " dimension-lists)
;        (println "new bounds: " new-bounds)
        new-bounds
        )
      )
    )
  )


(defn 

  reduce-fn 
  [f bounds step f-reducer]
  (let [points (get-points-ND bounds step)
        vals (map f points)
        ]
    (reduce f-reducer vals)
    )
  )

  
(defn rand-range 
  [lower upper]
  (+ (rand (- upper lower)) lower)
  )

(defn 
  ^{:test (fn[] 
            (assert (= (count (sample-ND (fn [point] (let [[x] point] ( + x)))  [[0 1]] 1))  1) )  
            (assert (= (count (sample-ND (fn [point] (let [[x y] point] ( + x y))) [[0 1] [0 1]] 1))  2) )  
            )}   
sample-ND 
  "Draw a sample from the probability function f between bounds. Assumes a probability density function with an area of 1."
  [f bounds max-random]
  (loop []
    (let [point (map #(rand-range (first %) (second %)) bounds)
          val (f point)
          rnd (* max-random (rand))]
      (if (<= rnd val) 
        point 
        (recur))
      )
    )
  )


(defn 
  umbrella-centers-1D 
  [bounds umbrella-spacing]
  
  (let [[lower upper] bounds]
    (inclusive-range-1D bounds  umbrella-spacing))
  )

(defn
  ^{:test (fn[] 
            (assert (= (umbrella-centers-ND [[-180 180]] [20])  '((-180) (-160) (-140) (-120) (-100) (-80) (-60) (-40) (-20) (0) (20) (40) (60) (80) (100) (120) (140) (160) (180)) ))  
            )}   
  umbrella-centers-ND
  [bounds umbrella-spacings]
  (apply combo/cartesian-product (map umbrella-centers-1D bounds umbrella-spacings))  
  
  )

(run-tests)



(defn 
  emulator
  ([dir-hist file-prefix pmf k T D H H-bounds UC K numeric-step no-samples]
    (let [
          ]

      (println "Generating samples....")
      
      (println "Input parameters ")
      (println "histogram root       : " dir-hist)
      (make-parents (str dir-hist "/dummy-file"))
      (println "file prefix          : " file-prefix)
      (println "temperature          : " T)
      (println "boltzmann constant   : " k)
      (println "dimensions           : " D)
      (println "number of histograms : " H)
      (println "bounds of histograms : " H-bounds)
      (println "umbrella centers     : " UC)
      (println "spring constants     : " K)
      (println "numeric eval step    : " numeric-step)
      (println "number of samples    : " no-samples)
      
      
      ;; write sample files
      (doseq [uc UC]
        (let [filename (str  dir-hist file-prefix (apply str (interpose "_" uc)) )
              umbrella-distribution (umbrella-probability-ND pmf k T K uc)
              umbrella-density (normalize umbrella-distribution H-bounds numeric-step)
              umbrella-sampling-bounds (reduce-bounds umbrella-density H-bounds numeric-step)
              max-random (* 2 (reduce-fn umbrella-density H-bounds numeric-step max))
              ]
          (print "Generating samples for histogram centered at: " uc)
          (print " between" umbrella-sampling-bounds " max-random " max-random " file: " filename )
          
          (if (not-empty umbrella-sampling-bounds)
            (with-open [wrt (io/writer filename)]
              (doseq [
                      sample (repeatedly no-samples #(sample-ND umbrella-density umbrella-sampling-bounds max-random))
                      ] 
                (.write wrt (str "0 " (apply str (interpose " " sample)) "\n"))
                )
              )
            )
          (println "  .....Done")
          )
        )
      )
    )
  )


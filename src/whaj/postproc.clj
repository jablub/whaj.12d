(ns whaj.postproc
  (:use [clojure.java.io :as io])
  (:require [clojure.math.numeric-tower :as math])
  (:use clojure.repl)
  (:use clojure.core.matrix)
  (:use whaj.emulator)
  (:use whaj.constants)
  (:use whaj.ccache)
  (:use whaj.common)
  )


(defn unboltzmann [k T prob]
  (let [ub (* -1 k T (Math/log prob))] 
    (if (= ub Double/POSITIVE_INFINITY) 
      -1
      ub)
    )
  )

(defn espit [filename ndarray]
  (with-open [wrt (io/writer filename)]
    (doseq [x (eseq ndarray)]
      (.write wrt (str x "\n")))))
  



(defn postproc [dir-prob pmf-function k T D H H-bounds B]
  
  (let [
        prob-file (str dir-prob "P")
        pmf-result-file (str dir-prob "PMF-result")
        pmf-reference-file (str dir-prob "PMF-reference")
             ]
    
    (println "Input parameters ")
    (println "dir-prob              : " dir-prob)
    (println "pmf-function         : ")
    (clojure.repl/source pmf-function )    
    (println "boltzmann constant   : " k)
    (println "temperature          : " T)
    (println "dimensions           : " D)
    (println "number of histograms : " H)
    (println "bounds of histograms : " H-bounds)
    (println "number of bins       : " B)
    
    (println "Reading probability file        : " prob-file)
    
    (with-open [rdr (io/reader prob-file)]
      (println "Opened        : ")
      (let [
            lseq (line-seq rdr)
            ]
        
        (doall (map #(let [
                           val (Float/parseFloat %)
                           ]
                       (if (> val 0) 
                         (println val)
                         )
                       ) lseq))
        
            

            
;            distribution-ND (emap #(Float/parseFloat %) (apply array nested-seq-1D))

            
;            density-ND (normalise-probabilities distribution-ND)
            
            ;; create the result ndarray and if any values are negative make them max
;            pmf-result-ND (emap (partial unboltzmann k T) density-ND)
;            max-result-ND (ereduce max pmf-result-ND)
;            pmf-result-ND (emap #(if (< % 0) max-result-ND %) pmf-result-ND)
;            
;            ;; create reference ndarray
;            pmf-reference-1D (map pmf-function (bin-centers-ND H-bounds B))
;            pmf-reference-ND (apply array (reduce #(partition %2 %1) pmf-reference-1D B))
;
;            ;; drop result down to baseline of reference 
;            min-result-ND (ereduce min pmf-result-ND)
;            min-reference-ND (ereduce min pmf-reference-ND)
;            shift (- min-reference-ND min-result-ND) 
;            pmf-result-ND (emap #(+ shift  %) pmf-result-ND)
;
;            ;; scale the max values to meet
;            max-result-ND (ereduce max pmf-result-ND)
;            max-reference-ND (ereduce max pmf-reference-ND)
;            scale (/ max-reference-ND max-result-ND)
;            pmf-result-ND (emap #(* scale  %) pmf-result-ND)
;            
;            ;; calculate the RMSD
;            rmsd (math/sqrt (/ (esum (pow (sub pmf-result-ND pmf-reference-ND) 2)) (ecount pmf-reference-ND)))
;            nrmsd (/ rmsd (- max-result-ND min-result-ND))
            
        
;        ;; write pmf results
;        (println "Writing pmf result file        : " pmf-result-file)
;        (espit pmf-result-file pmf-result-ND)
;
;        (println "Writing pmf reference file     : " pmf-reference-file)
;        (espit pmf-reference-file pmf-reference-ND)
;        
;        ;; writing  metrics 
;        (println "Shift : " shift)
;        (println "Scale : " scale)
;        
;	;        (if (= D 1)
;	;          (sandbox.jmathplot/data-plot-1D "test" "x" "y" 
;	;                                          (flatten (bin-centers-ND  H-bounds B))
;	;                                          (list 
;	;                                            (list pmf-reference-ND)
;	;                                            (list pmf-result-ND)
;	;                                            )
;	;                                          )
;	;          )
;        
;        {:shift shift 
;         :scale scale
;         :rmsd rmsd
;         :nrmsd nrmsd}
        )
      )
    )
  )
  

;  (data-plot-2D "test" "x" "y" "z" 
;                      (flatten (bin-centers-ND [[-180 180]] [90]))
;                      (flatten (bin-centers-ND [[-180 180]] [90]))
;                      (list 
;                        (into-array (map double-array pmf-result-ND)) 
;                        (into-array (map double-array pmf-reference-ND))
;                        )
;                      )
    



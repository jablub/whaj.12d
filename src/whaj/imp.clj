(ns whaj.imp
  (use whaj.constants)
  (:use [clojure.java.io :as io])
  (:use [clojure.java.shell :as shell])
  )

;	    cout<<"usage: "<< argv[0] <<" <input dir> <output dir> <no of runs> <D> <H> <B> <tol>\n";
;     /home/andrew/git/whac/test_data/ /home/andrew/git/whac/test_data/seq_cpu/R1/ seq_cpu 1 1 4 4 0.0001

(defn run-imp 
  [name cmd dir-bin dir-prob version no-of-runs D H B tol max-iterations debug]
  
  (make-parents (str dir-prob "/dummy-file"))
  (let [
        command (str cmd " " dir-bin " " dir-prob " " version " " no-of-runs " " D " " H " " B " " tol " " max-iterations " " debug)
        commandfile (str dir-prob version "-" no-of-runs ".sh")
        ]
  (println "Running Implementation :: " name )
  (println "Command :: \n" command)
  (println "Writing command sh :: \n" commandfile)
  (spit commandfile command)
  (.setExecutable (new java.io.File commandfile) true )
;  (println "Running  :: " name )
;  (println (shell/sh cmd dir-bin dir-prob version (str no-of-runs) (str D) (str H) (str B) (str tol) (str max-iterations) (str debug)))
  (println "  .... Done")
  
    
    )
  
  )


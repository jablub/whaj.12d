(ns whaj.main
  (:use [clojure.java.io :as io])
  (:use [clojure.java.shell :as shell])
  (:use whaj.emulator)
  (:use whaj.constants)
  (:use whaj.ccache)
  (:use whaj.nbcache)
  (:use whaj.postproc)  
  (:use whaj.project-to-1D)  
  (:use whaj.common)  
  (:use whaj.imp)  
  
  )

(defn run-emulator [config-file & options]
  
  (let [
        root          (.getParent (as-file config-file))
        config        (load-file config-file)
        file-prefix   (config :file-prefix)
        filename-fn   (config :filename-fn)
        line-parse-fn (config :line-parse-fn)
        dir-hist-overide (config :dir-hist-overide)
        D             (config :D)
        k             (config :k)
        pmf-string    (slurp (str root "/" (config :pmf))) 
        pmf           (load-string pmf-string )
        HS            (config :HS)
        imp           (config :imp)
        opts          (set options)
        
        ]
    
      (println "Input parameters ")
      (println "options " options)
      (println "pmf                  : \n ------------------------------- \n" pmf-string "\n ------------------------------- \n")
      (println "root                 : " root)
      (println "file prefix          : " file-prefix)
      (println "dimensions           : " D)
      (println "boltzmann constant   : " k)
      (println "implementation       : " imp)
      
      
      (doseq [H-conf HS]
        (let [
              T              (H-conf :T             )
              H              (H-conf :H             )
              H-bounds       (H-conf :H-bounds      )
              UC             (H-conf :UC            )
              K              (H-conf :K             )
              numeric-step   (H-conf :numeric-step  )
              no-samples     (H-conf :no-samples    )
              BS             (H-conf :BS            ) 
              dir-hist       (str root (if dir-hist-overide (str dir-hist-overide) (str "/D" D "/" (dir-hist H K no-samples numeric-step))) )
              
              ]
          
          (println "\n ------------------------------- \nHistogram " H "\n" dir-hist  "\n ------------------------------- \n")
          (if (:em opts) (emulator dir-hist file-prefix pmf k T D H H-bounds UC K numeric-step no-samples))
          (println "\n *********************************************************************************************************** \n")

          (doseq [B BS]
            (let [
                  dir-bin             (str dir-hist  (dir-bin B)  )
                      imp             (H-conf :imp    )
                      name            (imp :name      )
                      cmd             (imp-map name   )
                      version         (imp :version   )
                      no-of-runs      (imp :no-ofruns )
                      tol             (imp :tol       )
                      max-iterations  (imp :max-iterations       )
                      debug           (imp :debug       )
                      dir-prob        (str dir-bin version "/R" no-of-runs "/")
                      ]
              (println "\n ------------------------------- \nHistogram " H "  Bins " B "\n" dir-bin "\n ------------------------------- \n")
              
              (if (:nb opts) (nbcache dir-hist filename-fn line-parse-fn D H H-bounds UC B))
              (println "\n *********************************************************************************************************** \n")
              
              (if (:c opts) (ccache dir-bin k T D H H-bounds B UC K))
              (println "\n *********************************************************************************************************** \n")
              
              (if (:imp opts) 
                (run-imp name cmd dir-bin dir-prob version no-of-runs D (reduce * H) (reduce * B) tol max-iterations debug)
                )
              (println "\n *********************************************************************************************************** \n")
              
              (if (:post opts)
                (let [
                      metrics (postproc dir-prob pmf k T D H H-bounds B)
                      metrics-file (str dir-prob "M")
                      metrics (merge metrics {:D D :H (dir-shape H) :B (dir-shape B) :K K :no-samples no-samples })
                      ]
;                  (println "Writing metrics file     : " metrics-file)
;                  (println metrics)
;                  (spit metrics-file metrics)
                  )
                )
              (println "\n *********************************************************************************************************** \n")
              (if (:proj opts) 
                (project-file H-bounds B UC (str dir-prob "P"))
                
;                (run-imp name cmd dir-bin dir-prob version no-of-runs D (reduce * H) (reduce * B) tol max-iterations debug)
                )
              (println "\n *********************************************************************************************************** \n")
              
              )
            )
          )
        )
      )
  )


;(run-emulator "/home/andrew/git/whaj/test_data/Butane-2D/config.clj" :nb :c)  
;(run-emulator "/media/daniel/sammy/whaj.12D/test_data/wham_12D_test.02/12D-config.clj"  :post)
;(run-emulator "/home/andrew/git/whaj.12D/test_data/wham_3D/3D-config.clj" :imp)
(run-emulator "/home/andrew/git/whaj.12D/test_data/wham_3D/3D-config.clj" :proj)

(ns whaj.project-to-1D
  (:use [clojure.java.io :as io])
  (:use clojure.test)
  )

; Concepts
; nd-shape - the number of dimensions and the size of each dimension in n-dimensional space. 
;            Written as a vector, where new dimensions are appended to the beginning  
;                                  [4] a 1D array of lenght 4
;                                  [4 5] a 2D array - 4 rows, 5 columns
;                                  [4 5 3] a 3D array - 3 deep, 5 rows, 3 columns, 
;
; nd-index - array index in n-dimensional space.
;            Written as a vector, starts at 0
;                                  [1 2 0] is valid nd-index of 3D array with shape [4 5 3].  [1 2 3] is out of bounds.
;
; nd-point - a point in n-dimensional space.
;            Written as a vector.
;
; flat-index - array index in 1-dimensional space, flattened in row major order
;              Written as a single value, starts at 0
;                                  7 is the flat index of nd-index [1 2] of 2D array with shape [4 5].
;
; nd-bounds - sets of min and max values in n-dimensional space.
;             Written as a vector of vectors. Each inner vector has two values [min max]. Each inner vector gives the bounds of a single dimension.
;                                  Bounds of example /doc/project-to-1D.svg are [[4 8] [10 20]]


(defn 
  ^{:test (fn[] 
            (assert (= (distance [2] [5]) 3.0) )  
            (assert (= (distance [2 2] [5 5]) 4.242640687119285) )  
            (assert (= (distance [4.5 12.5] [5.25 12.25]) 0.7905694150420949) )  
            )}
distance 
  "Distance bewteen two points in n-dimensional space"
  [a b] ;thanks c.grande
  (Math/sqrt (reduce + (map #(let [c (- %1 %2)] (* c c)) a b))))



(defn 
  ^{:test (fn[] 
            (assert (= (raise-index 0 [2]) '(0)) ) 
            (assert (= (raise-index 1 [2]) '(1)) ) 
            (assert (= (raise-index 2 [2 2]) '(1 0)) ) 
            (assert (= (raise-index 8 [2 2 3]) '(1 0 2)) ) 
            )}
raise-index 
  "Coverts a flat-index to an n-dimensional index given the nd-shape"
  [flat-index nd-shape]
  (let [
        q (quot flat-index (last nd-shape))
        r (list (mod  flat-index (last nd-shape)))
        ]
	   (if (empty? (rest nd-shape)) 
      r 
      (concat (raise-index q (butlast nd-shape)) r))
    )
  )



(defn 
  ^{:test (fn[] 
            (assert (= (get-widths [[10 20]] [5]) '(2) ) )  
            (assert (= (get-widths [[10 20] [4 8]] [5 4]) '(2 1) ) )  
            )}  
get-widths 
  "Gets the n-dimensional widths of n-dimensional bounds given the shape of the dimensions"
  [nd-bounds nd-shape]
  (map #(/ (- (second %1) (first %1)) %2 )  nd-bounds nd-shape )
  )



(defn
  ^{:test (fn[] 
            (assert (= (nd-index-to-center-point [0] [[0 1]] [1]) [0.5] ) )  
            (assert (= (nd-index-to-center-point [2 1] [[4 8] [10 20]] [4 5]) [6.5 13.0] ) )  
            )}     
nd-index-to-center-point
  "Given an nd-array with shape nd-shape, the values of the bounds are nd-bounds, returns the value at the center of the cell at nd-index"
  [nd-index nd-bounds nd-shape]
  (let [nd-widths (get-widths nd-bounds nd-shape)]
      (map #(+ (* %1  %2) (* 0.5 %2) (first %3)) nd-index nd-widths nd-bounds)       ; index * width + 1/2 width
    )
  )

(defn 
  ^{:test (fn[] 
            (assert (= (find-closest-nd-point [0.5] [[0]]) 0 ) )
            (assert (= (find-closest-nd-point [1.6 2.5] [[0 1] [1 2] [2 3]]) 2 ) )
            )}     
find-closest-nd-point
  "Finds the flat-index of the closest point in nd-point-list to the nd-point"
  [nd-point nd-point-list]
  (first (apply min-key second 
                (map-indexed vector (map #(distance nd-point %) nd-point-list)) 
                )
         )
  )

(def kbT 0.597)

(defn
  ^{:test (fn[] 
            (assert (= (pmf 31 30000) 4.10435437732701 ) )  
            )}     
  pmf [sum-of-p deltaV]
  (* -1 kbT (Math/log (/ sum-of-p deltaV )))
  )

(def π Math/PI)

(defn 
  ^{:test (fn[] 
            (assert (= (volume-12D 5) 3.2599188692739E8 ) )  
            (assert (= (volume-12D 2) 5469.236301228398 ) )  
            )}   
  volume-12D [R]
  (* (/ 1 720) (Math/pow π 6) (Math/pow R 12))
  )

(defn 
  volume-2D [R]
  (* π (Math/pow R 2))
  )

(defn 
  volume-3D [R]
  (* 4/3 π (Math/pow R 3))
  )


(defn 
  add-prob-to-uc 
  [len-val-pair uc-list uc-index]
  (dosync 
    (let [uc-ref (nth uc-list uc-index)]
      (ref-set uc-ref (conj @uc-ref len-val-pair))
      )
    )
  )

;(run-tests)

(defn project 
  [nd-bounds nd-shape centroids points]
  (let [
        centroid-list (map ref (repeat (count centroids) '()))  ; create a list of lists - an inner list for each centroid point - the inner list will contain 2-tuples with (length-to-point, value-of-point)
        flat-index (atom 0)                                     ; this keeps a count of the line number we are looking at - the flat-index
        non-zero-lines (atom 0)
        ]
    
;    (println "flat-index nd-index center-point nearest-centroid-index nearest-centroid-point length-to-nearest-centroid p")
    ; Populate the centroid-list
    (doseq [pstr points]                                           ; go thru the sequence of points
      (let [p (Double/parseDouble pstr)]
        (if (> p 0)                                               ; for each non zero point ::
          (let [
                nd-index (raise-index @flat-index nd-shape)                                         ; get the nd-index from the flat-index
                center-point (nd-index-to-center-point nd-index nd-bounds nd-shape)                 ; find the center point of the nd-index
                nearest-centroid-index (find-closest-nd-point center-point centroids)               ; find the index of the nearest centroid
                nearest-centroid-point (nth centroids nearest-centroid-index)                       ; get the point from that index
                length-to-nearest-centroid (distance center-point nearest-centroid-point)           ; get the distance between the point and the nearest cenroid
                ] 
            ;            (println @flat-index nd-index center-point nearest-centroid-index nearest-centroid-point length-to-nearest-centroid p) 
            (swap! non-zero-lines inc)
            (add-prob-to-uc (list length-to-nearest-centroid p) centroid-list nearest-centroid-index) ; add the length value pair to the cenroid list
            ) 
          )
        )
      (swap! flat-index inc)
      )
    
    (println "Read all points. No of lines : " @flat-index "No. of non-zero-lines" @non-zero-lines)
;    (println "Read all points. No of lines : " @flat-index )
    
    ; Get the
    (let [centroid-no (atom 0)]
      (doseq [centroid centroid-list]
;        (print "centroid-no:" @centroid-no "no-of-points:" (count @centroid)) 
        (if (> (count @centroid) 0)
          (let [
                sum-of-p (second (reduce #(list 0 (+ (second %1) (second %2))) @centroid ))
		              max-distance (first (reduce #(list (max (first %1) (first %2))) @centroid))
		              deltaV (volume-3D max-distance)
		              pmf (pmf sum-of-p deltaV)
		              ]
;		          (print "  ::   sum-of-p:" sum-of-p "max-distance:" max-distance "deltaV:"  deltaV "pmf:" pmf)
		          (print  pmf)
		          )
          )
        
        (swap! centroid-no inc)
        (println)
        )
      )
    )
  )




(def test-projection 
  (let [nd-bounds [[4 8] [10 20]]
        nd-shape [4 5]
        ucs [[5.25 12.25] [7.25 16.5]]
        points ["3" "6" "9" "0" "0" "3" "4" "7" "9" "0" "2" "4" "5" "10" "6" "0" "0" "3" "8" "4"]
        ]
    (project nd-bounds nd-shape ucs points)
    )
  )

(defn project-file [nd-bounds nd-shape centroids prob-file ]
  (println "bounds of histograms : " nd-bounds)
  (println "umbrella centers     : " centroids)
  (println "number of bins       : " nd-shape)
  (println "Reading probability file        : " prob-file)
  
  (with-open [rdr (io/reader prob-file)]
    (println "Opened        : ")
    (project nd-bounds nd-shape centroids (line-seq rdr))
    )
  )


;I think there're 31 windows so x0=0,...,30. 
;Let's do: count all the P(x') in the region centered by x0, and get another population P'(x0). 
;PMF=-kb*T*ln(P'(x0)/deltaV(x0)). 
;deltaV(x0) is the 12D volume of the region centered by x0 (this volume has to use some approximation to calculate).
;V12 = 1/720  pi^6  R^12


;               kbT   sum P   deltaV               sum P / deltaV      ln(sum P / deltaV)   -kbT * ln(sum P / deltaV)
;Centroid 1   0.597   31     384155.413512874   8.06965069593145E-005   -9.4248152679           5.6266147149
;Centroid 2   0.597   52     133476.813891457   0.0003895808            -7.8504393444           4.6867122886
;
;                                                                                  difference   0.9399024263


